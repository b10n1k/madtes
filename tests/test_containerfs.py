import os
import time
import re


def test_running(container):
    assert container.is_running()
    
# def test_logs(container):
#     for line in container.logs(follow=True):
#         print(line)

# def test_stdin(container):
#     assert container.logs_unicode() == ""
#     #container.write_to_stdin(message=b'echo space\n')
#     container.execute(["echo", "space"])
#     # give container time to process
#     time.sleep(0.2)
#     assert container.logs() == 'space'
#     assert container.logs_unicode() == 'space'
        
def test_processes(container):
    results = re.compile('^root.+1.+\/bin\/bash')
    for out in container.execute(['ps', 'aux'], blocking=False):
        print(type(out))
        if (results.match(out.decode('utf-8'))):
            assert true
        
    #print(pr1)
    #assert p1 == "root         1  0.0  0.0   4132  2884 ?        Ss   07:20   0:00 /bin/bash"

def test_db_backend(container):
    v = container.execute(['rpm', '--eval', '%_db_backend'], blocking=False)
    for u in v:
        print("v is %s" % u)

def test_fs(container):
    with container.mount() as fs:
        assert fs.file_is_present("/etc/os-release")
