import os

class TestClass:
    def test_one(self):
        x = "this"
        assert "h" in x

    def test_two(self):
        x = "hello"
        assert hasattr(x, "check")
        
    def test_three(self, container):
        print("hello")
        print(container)
        assert not container.is_running()
