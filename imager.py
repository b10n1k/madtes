import sys
import pytest
import argparse

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('--img', metavar='N', type=str,
                    help='the SUT container image')
parser.add_argument('--tag', metavar='N', type=str,
                    help='tag of container image')
parser.add_argument('--rootpath', type=str,
                    help='pytest rootpath')

if len(sys.argv) == 1:
    parser.print_help()
    sys.exit(1)

args = parser.parse_args()

#pytest.main(["--rootdir=$HOME/Documents/Code/imager/", "--subunit"])
pytest.main(["%s/imager/" % args.rootpath,
             "--full-trace",
             "--junitxml=report.xml",
             "--img=" + args.img,
             "--tag=" + args.tag,
             "--rootpath=" + args.rootpath])
